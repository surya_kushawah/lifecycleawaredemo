package com.wave.lifecycleawaredemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    TextView txtLogs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtLogs = findViewById(R.id.textLogs);
        Log.d(TAG, "Owner onCreate");
        getLifecycle().addObserver(new MainActivityObserver());

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "Owner onStart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "Owner onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "Owner onResume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "Owner onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Owner onDestroy");

    }
}
